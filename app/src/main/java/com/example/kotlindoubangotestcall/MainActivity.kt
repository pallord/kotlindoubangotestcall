package com.example.kotlindoubangotestcall

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import org.doubango.ngn.NgnApplication
import org.doubango.ngn.NgnEngine
import org.doubango.ngn.events.*
import org.doubango.ngn.media.NgnMediaType
import org.doubango.ngn.services.INgnConfigurationService
import org.doubango.ngn.services.INgnSipService
import org.doubango.ngn.sip.NgnAVSession
import org.doubango.ngn.utils.NgnConfigurationEntry
import org.doubango.ngn.utils.NgnUriUtils

class MainActivity : AppCompatActivity() {

    private lateinit var mTvLog: TextView

    private var mSipBroadCastRecv: BroadcastReceiver? = null

    private lateinit var mEngine: NgnEngine
    private lateinit var mConfigurationService: INgnConfigurationService
    private lateinit var mSipService: INgnSipService

    private val IS_FIRST = true

    private val SIP_DOMAIN = "iptel.org"
    private val SIP_USERNAME = if (IS_FIRST) "82233575381@iptel.org" else "82233575137@iptel.org"
    private val SIP_ABONENT = if (IS_FIRST) "82233575137@iptel.org" else "82233575381@iptel.org"
    private val SIP_PASSWORD = "gfhjkm1"
    private val SIP_SERVER_HOST = "sip.iptel.org"
    private val SIP_SERVER_PORT = 5060

    companion object {
        val TAG = this::class.java.canonicalName
        val EXTRAT_SIP_SESSION_ID = "SipSession"
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mEngine = NgnEngine.getInstance()
        mConfigurationService = mEngine.getConfigurationService()
        mSipService = mEngine.getSipService()

        mTvLog = findViewById(R.id.main_textView_log) as TextView

        val etSipNumber:EditText = findViewById(R.id.etSIPNumber)
        val button:Button = findViewById(R.id.main_item_button_call)

        etSipNumber.setText(SIP_ABONENT)
        button.setOnClickListener{
            if(!etSipNumber.text.isNullOrEmpty())
                makeVoiceCall(etSipNumber.text.toString())
        }

        mTvLog.text = "onCreate()"

        // Listen for registration events
        mSipBroadCastRecv = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action

                // Registration Event
                if (NgnRegistrationEventArgs.ACTION_REGISTRATION_EVENT.equals(action)) {
                    val args:NgnRegistrationEventArgs = intent.getParcelableExtra(NgnEventArgs.EXTRA_EMBEDDED)
                    when (args.eventType) {
                        NgnRegistrationEventTypes.REGISTRATION_NOK -> mTvLog.text = "Failed to register"
                        NgnRegistrationEventTypes.UNREGISTRATION_OK -> mTvLog.text = "You are now unregistered"
                        NgnRegistrationEventTypes.REGISTRATION_OK -> mTvLog.text = "You are now registered"
                        NgnRegistrationEventTypes.REGISTRATION_INPROGRESS -> mTvLog.text = "Trying to register..."
                        NgnRegistrationEventTypes.UNREGISTRATION_INPROGRESS -> mTvLog.text = "Trying to unregister..."
                        NgnRegistrationEventTypes.UNREGISTRATION_NOK -> mTvLog.text = "Failed to unregister"
                    }
                } else if (NgnInviteEventArgs.ACTION_INVITE_EVENT == action) {
                    val args =
                        intent.getParcelableExtra<NgnInviteEventArgs>(NgnEventArgs.EXTRA_EMBEDDED)
                    if (args == null) {
                        Log.e(TAG, "Invalid event args")
                        return
                    }

                    val mediaType = args.mediaType

                    when (args.eventType) {
                        NgnInviteEventTypes.TERMWAIT, NgnInviteEventTypes.TERMINATED -> if (NgnMediaType.isAudioVideoType(
                                mediaType
                            )
                        ) {
                            mEngine.soundService.stopRingBackTone()
                            mEngine.soundService.stopRingTone()
                        }

                        NgnInviteEventTypes.INCOMING -> if (NgnMediaType.isAudioVideoType(mediaType)) {
                            val avSession = NgnAVSession.getSession(args.sessionId)
                            if (avSession != null) {
                                startActivity(IncomingCallActivity
                                    .getIntent(NgnApplication.getContext(),avSession.id))
                                mEngine.soundService.startRingTone()
                            } else {
                                Log.e(TAG,
                                    String.format("Failed to find session with id=%ld", args.sessionId))
                            }
                        }

                        NgnInviteEventTypes.INPROGRESS ->
                            if (NgnMediaType.isAudioVideoType(mediaType)) {
                                Log.d(TAG,"event: inprogress")
                            }

                        NgnInviteEventTypes.RINGING ->
                            if (NgnMediaType.isAudioVideoType(mediaType)) {
                                mEngine.soundService.startRingBackTone()
                                Log.d(TAG,"event: ringing")
                            }

                        NgnInviteEventTypes.CONNECTED, NgnInviteEventTypes.EARLY_MEDIA ->
                            if (NgnMediaType.isAudioVideoType(mediaType)) {
                                mEngine.soundService.stopRingBackTone()
                                mEngine.soundService.stopRingTone()
                                Log.d(TAG,"event: connected, early media")
                            }
                        else -> {
                        }
                    }
                }
            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(NgnRegistrationEventArgs.ACTION_REGISTRATION_EVENT)
        intentFilter.addAction(NgnInviteEventArgs.ACTION_INVITE_EVENT)
        registerReceiver(mSipBroadCastRecv, intentFilter)
    }


    override fun onDestroy() {
        // Stops the engine
        if (mEngine.isStarted()) {
            mEngine.stop()
        }
        // release the listener
        if (mSipBroadCastRecv != null) {
            unregisterReceiver(mSipBroadCastRecv)
            mSipBroadCastRecv = null
        }
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        // Starts the engine
        if (!mEngine.isStarted()) {
            if (mEngine.start()) {
                mTvLog.text = "Engine started"
            } else {
                mTvLog.text = "Failed to start the engine"
            }
        }
        // Register
        if (mEngine.isStarted()) {
            if (!mSipService.isRegistered()) {
                // Set credentials
                mConfigurationService.putString(NgnConfigurationEntry.IDENTITY_IMPI, SIP_USERNAME)
                mConfigurationService.putString(
                    NgnConfigurationEntry.IDENTITY_IMPU,
                    String.format("sip:%s@%s", SIP_USERNAME, SIP_DOMAIN)
                )
                mConfigurationService.putString(
                    NgnConfigurationEntry.IDENTITY_PASSWORD,
                    SIP_PASSWORD
                )
                mConfigurationService.putString(
                    NgnConfigurationEntry.NETWORK_PCSCF_HOST,
                    SIP_SERVER_HOST
                )
                mConfigurationService.putInt(
                    NgnConfigurationEntry.NETWORK_PCSCF_PORT,
                    SIP_SERVER_PORT
                )
                mConfigurationService.putString(NgnConfigurationEntry.NETWORK_REALM, SIP_DOMAIN)
                // VERY IMPORTANT: Commit changes
                mConfigurationService.commit()
                // register (log in)
                mSipService.register(this)
            }
        }
    }

    internal fun makeVoiceCall(phoneNumber: String): Boolean {
        val validUri =
            NgnUriUtils.makeValidSipUri(String.format("sip:%s@%s", phoneNumber, SIP_DOMAIN))
        if (validUri == null) {
            mTvLog.text = "failed to normalize sip uri '$phoneNumber'"
            return false
        }
        val avSession =
            NgnAVSession.createOutgoingSession(mSipService.getSipStack(), NgnMediaType.Audio)

        val i = Intent()
        i.setClass(this, OutgoingCallActivity::class.java)
        i.putExtra(EXTRAT_SIP_SESSION_ID, avSession.getId())
        startActivity(i)

        return avSession.makeCall(validUri)
    }
}
